# Build web application
FROM node:lts-alpine as web-app-build

# following configuration is only for HL development machine
# RUN yarn config set registry https://nexus.hltech.dev/repository/npm
# RUN yarn config set disable-self-update-check true
# RUN yarn config set strict-ssl false

WORKDIR /app
COPY . ./

RUN yarn install
RUN yarn build

# Run application
FROM nginx:alpine
COPY  --from=web-app-build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]