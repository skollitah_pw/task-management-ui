export interface ProjectDTO {
    id: string;
    name: string;
    created_at?: Date;
    last_updated_at?: Date;
    tasks: TaskDTO[];
}

export interface TaskDTO {
    id: string;
    name: string;
    details: string;
    status: TaskStatusDTOEnum;
    created_at?: Date;
    last_updated_at?: Date;
}

export enum TaskStatusDTOEnum {
    TO_DO = 'To Do',
    IN_PROGRESS = 'In Progress',
    DONE = 'Completed',
    HOLD = 'Hold',
}

export interface AddTaskDTO {
    name: string;
    details: string;
    status: TaskStatusDTOEnum;
}

export interface AddProjectDTO {
    name: string;
}
