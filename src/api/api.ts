import Axios from "axios"
import {AddProjectDTO, AddTaskDTO, ProjectDTO, TaskDTO} from "./model";
import {TASK_MANAGEMENT_BACKEND_URL} from "../config";

export async function callGetAllProjects(): Promise<ProjectDTO[]> {
    const response = await Axios.get<ProjectDTO[]>(`${TASK_MANAGEMENT_BACKEND_URL}/projects`);
    return response.data;
}

export async function callAddTask(projectId: string, addTaskDTO: AddTaskDTO): Promise<TaskDTO> {
    const response = await Axios.post<AddTaskDTO, any>(`${TASK_MANAGEMENT_BACKEND_URL}/projects/${projectId}/tasks`, addTaskDTO);
    return response.data;
}

export async function callCompleteTask(taskId: string): Promise<TaskDTO> {
    const response = await Axios.patch<TaskDTO>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}/complete`);
    return response.data;
}

export async function callToDoTask(taskId: string): Promise<TaskDTO> {
    const response = await Axios.patch<TaskDTO>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}/to_do`);
    return response.data;
}

export async function callInProgressTask(taskId: string): Promise<TaskDTO> {
    const response = await Axios.patch<TaskDTO>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}/in_progress`);
    return response.data;
}

export async function callOnHoldTask(taskId: string): Promise<TaskDTO> {
    const response = await Axios.patch<TaskDTO>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}/on_hold`);
    return response.data;
}

export async function callAddProject(addProjectDTO: AddProjectDTO): Promise<ProjectDTO> {
    const response = await Axios.post<ProjectDTO, any>(`${TASK_MANAGEMENT_BACKEND_URL}/projects`, addProjectDTO);
    return response.data;
}

export async function callDeleteProject(projectId: string): Promise<void> {
    await Axios.delete(`${TASK_MANAGEMENT_BACKEND_URL}/projects/${projectId}`);
}

export async function callEditTask(taskId: string, addTaskDTO: AddTaskDTO): Promise<TaskDTO> {
    const response = await Axios.put<AddTaskDTO, any>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}`, addTaskDTO);
    return response.data;
}

export async function callEditProject(projectId: string, addProjectDTO: AddProjectDTO): Promise<ProjectDTO> {
    const response = await Axios.put<AddProjectDTO, any>(`${TASK_MANAGEMENT_BACKEND_URL}/projects/${projectId}`, addProjectDTO);
    return response.data;
}

export async function callDeleteTask(taskId: string): Promise<void> {
    await Axios.delete<ProjectDTO, any>(`${TASK_MANAGEMENT_BACKEND_URL}/tasks/${taskId}`);
}