import {Project, Task, TaskStatusEnum} from "../store/model";
import {AddProjectDTO, AddTaskDTO, ProjectDTO, TaskDTO, TaskStatusDTOEnum} from "./model";
import {TaskForm} from "../components/TaskFormDialog";
import {ProjectForm} from "../components/ProjectDialog";

export const mapProjectDTOs = (projectDTOs: ProjectDTO[]): Project[] => {
    return projectDTOs.map(projectDTO => {
        return mapProjectDTO(projectDTO);
    });
};

export const mapProjectDTO = (projectDTO: ProjectDTO): Project => {
    return {
        id: projectDTO.id,
        name: projectDTO.name,
        created_at: projectDTO.created_at,
        last_updated_at: projectDTO.last_updated_at,
        tasks: mapTaskDTOs(projectDTO.tasks),
    };
};

export const mapTaskDTOs = (taskDTOs: TaskDTO[]): Task[] => {
    return taskDTOs.map(taskDTO => {
        return mapTaskDTO(taskDTO);
    });
};

export const mapTaskDTO = (taskDTO: TaskDTO): Task => {
    return {
        id: taskDTO.id,
        name: taskDTO.name,
        details: taskDTO.details,
        status: mapTaskStatusDTO(taskDTO.status),
        created_at: taskDTO.created_at,
        last_updated_at: taskDTO.last_updated_at
    };
};

const mapTaskStatusDTO = (taskStatusDTO: TaskStatusDTOEnum): TaskStatusEnum => {
    let taskStatusEnum: TaskStatusEnum;

    switch (taskStatusDTO) {
        case TaskStatusDTOEnum.TO_DO:
            taskStatusEnum = TaskStatusEnum.TO_DO;
            break;
        case TaskStatusDTOEnum.DONE:
            taskStatusEnum = TaskStatusEnum.DONE;
            break;
        case TaskStatusDTOEnum.HOLD:
            taskStatusEnum = TaskStatusEnum.HOLD;
            break;
        case TaskStatusDTOEnum.IN_PROGRESS:
            taskStatusEnum = TaskStatusEnum.IN_PROGRESS;
            break;
    }

    return taskStatusEnum;
};

export const mapTaskForm = (taskForm: TaskForm): AddTaskDTO => {
    return {
        ...taskForm,
        status: TaskStatusDTOEnum.TO_DO,
    }
};

export const mapAddProjectForm = (addProjectForm: ProjectForm): AddProjectDTO => {
    return {
        ...addProjectForm,
    }
};