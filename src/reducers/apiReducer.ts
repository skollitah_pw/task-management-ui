import {ApiState} from "../store/model";
import {Reducer} from "redux";
import produce, {Draft} from "immer";
import {ApiActions, ApiActionTypes} from "../actions/apiActions";

const initialState: ApiState = {
    callsInProgress: 0,
};

export const apiReducer: Reducer<ApiState, ApiActionTypes> = (state = initialState, action) => {
    return produce(state, (draft: Draft<ApiState>): void => {
        switch (action.type) {
            case ApiActions.API_CALL_START:
                draft.callsInProgress += 1;
                break;
            case ApiActions.API_CALL_FAILURE:
                draft.callsInProgress -= 1;
                break;
            // TODO api call finish
            default:
                break;
        }
    });
};