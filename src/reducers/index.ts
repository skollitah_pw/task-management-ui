import {combineReducers} from 'redux';
import {projectReducer} from './projectReducer';
import {AppStore} from "../store/model";
import {ThunkAction, ThunkDispatch} from "redux-thunk";
import {ProjectActionTypes} from "../actions/projectActions";
import {TaskActionTypes} from "../actions/taskActions";
import {ApiActionTypes} from "../actions/apiActions";
import {apiReducer} from "./apiReducer";

export type AllActionTypes = ProjectActionTypes | TaskActionTypes | ApiActionTypes

export type StoreDispatch = ThunkDispatch<AppStore, void, AllActionTypes>
export type StoreThunkAction = ThunkAction<any, AppStore, void, AllActionTypes>

export const rootReducer = combineReducers<AppStore>({
    projectState: projectReducer,
    apiState: apiReducer,
});

