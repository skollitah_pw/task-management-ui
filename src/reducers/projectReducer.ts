import {ProjectActions, ProjectActionTypes} from '../actions/projectActions';
import {Reducer} from "redux";
import {Project, ProjectsState, Task, TaskStatusEnum} from "../store/model";
import produce, {Draft} from "immer";
import {mapProjectDTO, mapProjectDTOs, mapTaskDTO} from "../api/mappers";
import {TaskActions, TaskActionTypes} from "../actions/taskActions";
import {remove} from 'lodash';
import {TaskDTO} from "../api/model";

const initialState: ProjectsState = {
    projects: [],
};

export const projectReducer: Reducer<ProjectsState, ProjectActionTypes | TaskActionTypes> = (state = initialState, action) => {

    const findTask = (draft: Draft<ProjectsState>, projectId: string, taskId: string): Task | undefined => {
        let project = draft.projects.find(project => project.id === projectId);
        if (project !== undefined) {
            return project.tasks.find(task => task.id === taskId);
        }
    };

    const updateTaskStatus = (draft: Draft<ProjectsState>, projectId: string, taskId: string, newStatus: TaskStatusEnum) => {
        const task = findTask(draft, projectId, taskId);

        if (task !== undefined) {
            task.status = newStatus;
        }
    };

    const updateTask = (draft: Draft<ProjectsState>, projectId: string, taskId: string, taskDTO: TaskDTO) => {
        const task = findTask(draft, projectId, taskId);

        if (task !== undefined) {
            task.name = taskDTO.name;
            task.details = taskDTO.details;
        }
    };

    return produce(state, (draft: Draft<ProjectsState>): void => {
        switch (action.type) {
            case ProjectActions.LOAD_PROJECTS_SUCCESS:
                draft.projects = mapProjectDTOs(action.payload.dto);
                break;
            case ProjectActions.ADD_PROJECT_SUCCESS:
                draft.projects.push(mapProjectDTO(action.payload.project));
                break;
            case ProjectActions.DELETE_PROJECT_SUCCESS:
                remove(draft.projects, (project: Project) => project.id === action.payload.projectId)
                break;
            case ProjectActions.EDIT_PROJECT_SUCCESS:
                let project = draft.projects.find(project => project.id === action.payload.projectId);

                if (project !== undefined) {
                    project.name = action.payload.projectDTO.name;
                }

                break;
            case TaskActions.ADD_TASK_SUCCESS: {
                let project = draft.projects.find(project => project.id === action.payload.projectId);

                if (project !== undefined) {
                    project.tasks.push(mapTaskDTO(action.payload.task));
                }
                break;
            }
            case TaskActions.DELETE_TASK_SUCCESS: {
                let project = draft.projects.find(project => project.id === action.payload.projectId);

                if (project !== undefined) {
                    remove(project.tasks, (task: Task) => task.id === action.payload.taskId);
                }

                break;
            }
            case TaskActions.EDIT_TASK_SUCCESS: {
                updateTask(draft, action.payload.projectId, action.payload.taskId, action.payload.task);
                break;
            }
            case TaskActions.COMPLETE_TASK_SUCCESS:
                updateTaskStatus(draft, action.payload.projectId, action.payload.taskId, TaskStatusEnum.DONE);
                break;
            case TaskActions.TO_DO_TASK_SUCCESS:
                updateTaskStatus(draft, action.payload.projectId, action.payload.taskId, TaskStatusEnum.TO_DO);
                break;
            case TaskActions.IN_PROGRESS_TASK_SUCCESS:
                updateTaskStatus(draft, action.payload.projectId, action.payload.taskId, TaskStatusEnum.IN_PROGRESS);
                break;
            case TaskActions.ON_HOLD_TASK_SUCCESS:
                updateTaskStatus(draft, action.payload.projectId, action.payload.taskId, TaskStatusEnum.HOLD);
                break;
            default:
                break;
        }
    });
};