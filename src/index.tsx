import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./components/App";
import {Provider} from "react-redux";
import {configureStore} from "./store";
import {AppStore, TaskStatusEnum} from "./store/model";

const initialAppStore: AppStore = {
    projectState: {
        projects: [
            {
                id: "91b1f2c7-0d0d-439f-8dba-78d829ce49de",
                name: "Project 1",
                created_at: new Date(),
                last_updated_at: new Date(),
                tasks: [
                    {
                        id: "12b1f2c7-0d0d-439f-8dba-78d829ce49de",
                        name: 'Task 1',
                        details: 'task 1 description',
                        created_at: new Date(),
                        last_updated_at: new Date(),
                        status: TaskStatusEnum.IN_PROGRESS,
                    },
                ],
            },
        ],
    },
    apiState: {
        callsInProgress: 0,
    }
};

const store = configureStore(initialAppStore);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById("root"));