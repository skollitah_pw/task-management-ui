import React from 'react';
import {Button, Dialog, DialogActions, DialogTitle, TextField} from '@material-ui/core';
import {Form, Formik} from "formik";
import * as Yup from "yup"
import DialogContent from "@material-ui/core/DialogContent";
import {FormikErrors, FormikProps} from "formik/dist/types";

export interface TaskFormDialogProps {
    initialValues?: TaskForm,
    dialogTitle: string;
    submitButtonTitle: string;
    open: boolean;
    handleClose: () => void;
    handleSubmit: (taskForm: TaskForm) => void;
}

export interface TaskForm {
    name: string,
    details: string,
}

export const TaskFormDialog: React.FC<TaskFormDialogProps> = (
    {
        open,
        dialogTitle,
        submitButtonTitle,
        handleClose,
        handleSubmit,
        initialValues = {
            name: "",
            details: ""
        },
    }) => {

    const preventDefault = (event: React.SyntheticEvent) => event.preventDefault();

    const handleSubmitAndClose = (submitForm: (params: any) => any, errors: FormikErrors<TaskForm>): any => {

        if (errors.details || errors.name) {
            return;
        }

        return (params: any): any => {
            preventDefault(params);
            handleClose();
            submitForm(params);
        };
    };

    return (
        <div>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
                <DialogContent>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={handleSubmit}
                        validationSchema={Yup.object().shape({
                            name: Yup.string()
                                .required('Required'),
                            details: Yup.string()
                                .required('Required'),
                        })}
                    >
                        {(props: FormikProps<TaskForm>) => {
                            const {
                                values,
                                touched,
                                errors,
                                handleChange,
                                handleBlur,
                                submitForm,
                            } = props;
                            return (
                                <Form>
                                    <DialogContent>
                                        <TextField
                                            autoFocus
                                            fullWidth={true}
                                            error={touched.name && Boolean(errors.name)}
                                            margin="dense"
                                            id="name"
                                            label="Task name"
                                            type="text"
                                            value={values.name}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={touched.name ? errors.name : ""}
                                        />
                                        <TextField
                                            multiline={true}
                                            fullWidth={true}
                                            error={touched.details && Boolean(errors.details)}
                                            margin="dense"
                                            id="details"
                                            label="Task Details"
                                            type="text"
                                            value={values.details}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={touched.details ? errors.details : ""}
                                        />
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClose} color="primary">
                                            Cancel
                                        </Button>
                                        <Button type="submit" onClick={handleSubmitAndClose(submitForm, errors)}
                                                color="primary" variant={"contained"}>
                                            {submitButtonTitle}
                                        </Button>
                                    </DialogActions>
                                </Form>
                            )
                        }}
                    </Formik>
                </DialogContent>
            </Dialog>
        </div>
    );
};
