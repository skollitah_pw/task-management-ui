import {
    Button,
    ButtonGroup,
    Divider,
    ExpansionPanel,
    ExpansionPanelActions,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    Grid
} from "@material-ui/core";
import React from "react";
import {Task, TaskStatusEnum} from "../store/model";
import {useDispatch} from "react-redux";
import {completeTask, deleteTask, editTask, inProgressTask, onHoldTask, toDoTask} from "../actions/taskActions";
import {makeStyles} from "@material-ui/core/styles";
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {TaskForm, TaskFormDialog} from "./TaskFormDialog";
import {mapTaskForm} from "../api/mappers";


export interface TaskRecordProps {
    projectId: string;
    task: Task;
    key: string;
}

interface TaskPanelStylesProps {
    status: TaskStatusEnum,
}

const makeTaskPanelStyles = (props: TaskPanelStylesProps) => makeStyles({
    root: {
        borderColor: ((status: TaskStatusEnum) => {
            switch (status) {
                case     TaskStatusEnum.TO_DO:
                    return 'LightSalmon';
                case     TaskStatusEnum.IN_PROGRESS:
                    return 'lightgreen';
                case     TaskStatusEnum.DONE:
                    return 'lightgray';
                case     TaskStatusEnum.HOLD:
                    return 'gray';
            }
        })(props.status),
        backgroundColor: ((status: TaskStatusEnum) => {
            switch (status) {
                case     TaskStatusEnum.TO_DO:
                    return 'LightSalmon';
                case     TaskStatusEnum.IN_PROGRESS:
                    return 'lightgreen';
                case     TaskStatusEnum.DONE:
                    return 'lightgray';
                case     TaskStatusEnum.HOLD:
                    return 'gray';
            }
        })(props.status),
        borderWidth: 2,
        textDecoration: ((status: TaskStatusEnum) => status === TaskStatusEnum.DONE ? 'line-through' : undefined)(props.status),
    },
    expansionSummary: {
        textAlign: 'right',
    },
});

export const TaskRecord: React.FC<TaskRecordProps> = ({projectId, task}) => {
    const dispatch = useDispatch();
    const taskPanelStyles = makeTaskPanelStyles({status: task.status})();

    const handleCompleteTask = (projectId: string, taskId: string): () => any => {
        return () => dispatch(completeTask(projectId, taskId));
    };

    const handleToDoTask = (projectId: string, taskId: string): () => any => {
        return () => dispatch(toDoTask(projectId, taskId));
    };

    const handleInProgressTask = (projectId: string, taskId: string): () => any => {
        return () => dispatch(inProgressTask(projectId, taskId));
    };

    const handleOnHoldTask = (projectId: string, taskId: string): () => any => {
        return () => dispatch(onHoldTask(projectId, taskId));
    };

    const handleDelete = (projectId: string, taskId: string): () => any => {
        return () => dispatch(deleteTask(projectId, taskId));
    };

    const [open, setOpen] = React.useState<boolean>(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleEditTask = (taskForm: TaskForm): void => {
        dispatch(editTask(projectId, task.id, mapTaskForm(taskForm)))
    };

    const initialValues = {name: task.name, details: task.details};

    return (
        <Grid container spacing={1}>
            <Grid item xs={9}>
                <ExpansionPanel variant="outlined" className={taskPanelStyles.root}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        {task.name}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        {task.details}
                    </ExpansionPanelDetails>
                    <Divider/>
                    <ExpansionPanelActions>
                        <EditOutlinedIcon onClick={handleClickOpen}/>
                        <DeleteForeverOutlinedIcon onClick={handleDelete(projectId, task.id)}/>
                    </ExpansionPanelActions>
                </ExpansionPanel>
            </Grid>
            <Grid item xs={3}>
                <ButtonGroup size="small" color="primary" variant={"contained"}>
                    {task.status === TaskStatusEnum.IN_PROGRESS &&
                    <Button onClick={handleCompleteTask(projectId, task.id)}>Complete</Button>}
                    {task.status === TaskStatusEnum.TO_DO &&
                    <Button onClick={handleInProgressTask(projectId, task.id)}>In Progress</Button>}
                    {(task.status === TaskStatusEnum.DONE || task.status === TaskStatusEnum.HOLD) &&
                    <Button onClick={handleToDoTask(projectId, task.id)}>To Do</Button>}
                    {(task.status === TaskStatusEnum.IN_PROGRESS || task.status === TaskStatusEnum.TO_DO) &&
                    <Button onClick={handleOnHoldTask(projectId, task.id)}>On Hold</Button>}
                </ButtonGroup>
                <TaskFormDialog
                    open={open}
                    dialogTitle={"Edit task"}
                    submitButtonTitle={"Confirm"}
                    handleClose={handleClose}
                    handleSubmit={handleEditTask}
                    initialValues={initialValues}/>
            </Grid>
        </Grid>
    );
};
