import {Divider, Tab} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/styles";
import {ProjectDialog, ProjectForm} from "./ProjectDialog";
import {deleteProject, editProject} from "../actions/projectActions";
import {mapAddProjectForm} from "../api/mappers";
import {useDispatch} from "react-redux";
import {ProjectMenu} from "./ProjectMenu";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

interface ProjectHeaderProps {
    projectId: string,
    key: string,
    projectName: string,
    index: number,
    props?: any[]
}

const useStyles = makeStyles({
    root: {
        fontSize: 15,
        marginRight: 0,
    },
    buttonIcon: {
        marginTop: 13,
        marginLeft: 0,
    },
});

const a11yProps = (index: number) => {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
};

export const ProjectHeader: React.FC<ProjectHeaderProps> = ({projectId, projectName, index, ...props}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState<boolean>(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const dispatch = useDispatch();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpenAnchor = (event: any) => {
        setAnchorEl(event.currentTarget);
    };

    const handleCloseAnchor = () => {
        setAnchorEl(null);
    };

    const handleEditProject = (projectForm: ProjectForm): void => {
        dispatch(editProject(projectId, mapAddProjectForm(projectForm)))
    };

    const handleDelete = (projectId: string): () => any => {
        return () => dispatch(deleteProject(projectId));
    };

    return (
        <>
            <Tab
                contextMenu={"menu"}
                className={classes.root}
                label={projectName}
                {...a11yProps(index)}
                {...props} />
            <ExpandMoreIcon className={classes.buttonIcon} onClick={handleClickOpenAnchor}/>
            <ProjectMenu
                handleClose={handleCloseAnchor}
                anchorEl={anchorEl}
                handleDelete={handleDelete(projectId)}
                handleEdit={handleClickOpen}
            />
            <ProjectDialog
                initialValues={{name: projectName}}
                open={open}
                handleSubmit={handleEditProject}
                handleClose={handleClose}
                dialogTitle={"Edit Project"}
                submitButtonTitle={"Confirm"}/>
            <Divider orientation={"vertical"}/>
        </>
    );
};