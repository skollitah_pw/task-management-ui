import {Form, Formik} from "formik";
import * as Yup from "yup";
import {FormikErrors, FormikProps} from "formik/dist/types";
import TextField from "@material-ui/core/TextField";
import {Button, Dialog, DialogActions, DialogTitle} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import DialogContent from "@material-ui/core/DialogContent";

export interface AddProjectFormProps {
    initialValues?: ProjectForm,
    dialogTitle: string;
    submitButtonTitle: string;
    open: boolean;
    handleClose: () => void;
    handleSubmit: (projectForm: ProjectForm) => void;
}

export interface ProjectForm {
    name: string,
}

const makeFormStyles = () => makeStyles({
    root: {
        marginTop: 5,
        backgroundColor: "green",
    },
    textArea: {
        backgroundColor: "white",
    },
});

export const ProjectDialog: React.FC<AddProjectFormProps> = (
    {
        open,
        dialogTitle,
        submitButtonTitle,
        handleClose,
        handleSubmit,
        initialValues = {
            name: "",
        },
    }) => {

    const formStyles = makeFormStyles()();

    const preventDefault = (event: React.SyntheticEvent) => event.preventDefault();

    const handleSubmitAndClose = (submitForm: (params: any) => any, errors: FormikErrors<ProjectForm>): any => {

        if (errors.name) {
            return;
        }

        return (params: any): any => {
            preventDefault(params);
            handleClose();
            submitForm(params);
        };
    };

    return (
        <div>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
                <DialogContent>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={handleSubmit}
                        validationSchema={Yup.object().shape({
                            name: Yup.string()
                                .required('Required')
                        })}>
                        {(props: FormikProps<ProjectForm>) => {
                            const {
                                values,
                                touched,
                                errors,
                                handleChange,
                                handleBlur,
                                submitForm,
                            } = props;
                            return (
                                <Form>
                                    <DialogContent>
                                        <TextField
                                            className={formStyles.textArea}
                                            error={touched.name && Boolean(errors.name)}
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Project Name"
                                            type="text"
                                            value={values.name}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={touched.name ? errors.name : ""}
                                        />
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClose} color="primary">
                                            Cancel
                                        </Button>
                                        <Button type="submit" onClick={handleSubmitAndClose(submitForm, errors)}
                                                color="primary" variant={"contained"}>
                                            {submitButtonTitle}
                                        </Button>
                                    </DialogActions>
                                </Form>
                            )
                        }}
                    </Formik>
                </DialogContent>
            </Dialog>
        </div>
    );
};
