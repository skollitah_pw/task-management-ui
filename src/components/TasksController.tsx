import {Box, Fab, Grid, Typography} from "@material-ui/core";
import React from "react";
import {Task} from "../store/model";
import {TaskRecord} from "./TaskRecord";
import {TaskForm, TaskFormDialog} from "./TaskFormDialog";
import AddIcon from "@material-ui/icons/Add";
import {makeStyles} from "@material-ui/core/styles";
import {addTask} from "../actions/taskActions";
import {mapTaskForm} from "../api/mappers";
import {useDispatch} from "react-redux";

export interface TasksControllerProps {
    key: string,
    projectId: string,
    tasks: Task[],
    index: number;
    value: any;
    other?: any[];
}

interface TabPanelProps {
    index: number;
    value: any;
    children?: React.ReactNode;
}

const TabPanel = (props: TabPanelProps) => {
    const {value, index, children, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
};

const makeDialogStyles = () => makeStyles({
    root: {
        backgroundColor: "green"
    },
});

export const TasksController: React.FC<TasksControllerProps> = ({projectId, tasks, index, value, ...other}) => {
    const [open, setOpen] = React.useState<boolean>(false);
    const dialogStyles = makeDialogStyles()();
    const dispatch = useDispatch();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleAddTask = (taskForm: TaskForm): void => {
        dispatch(addTask(projectId, mapTaskForm(taskForm)))
    };

    return (
        <TabPanel value={value} index={index} {...other}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <Fab size={"medium"} className={dialogStyles.root} color="primary" onClick={handleClickOpen}>
                        <AddIcon/>
                    </Fab>
                    <TaskFormDialog
                        open={open}
                        dialogTitle={"Add Task"}
                        submitButtonTitle={"Add"}
                        handleClose={handleClose}
                        handleSubmit={handleAddTask}/>
                </Grid>
            </Grid>
            {tasks.map(task => (
                <TaskRecord key={task.id} projectId={projectId} task={task}/>
            ))}
        </TabPanel>
    )
};