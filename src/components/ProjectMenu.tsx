import React from "react";
import {Button, Menu, MenuItem} from "@material-ui/core";

export interface ProjectMenuProps {
    anchorEl: any,
    handleClose: () => void;
    handleEdit: () => void;
    handleDelete: () => void;
}

export const ProjectMenu: React.FC<ProjectMenuProps> = ({anchorEl, handleClose, handleEdit, handleDelete}) => {

    return (
        <>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleEdit}>Edit</MenuItem>
                <MenuItem onClick={handleDelete}>Delete</MenuItem>
            </Menu>
        </>
    );
};