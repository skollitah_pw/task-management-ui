import React, {useEffect} from "react";
import {AppBar, Fab, Tabs} from "@material-ui/core";
import {useDispatch, useSelector} from 'react-redux';
import {AppStore, Project, ProjectsState} from "../store/model";
import {projectsSelector} from "../store/selectors";
import {addProject, loadProjects} from "../actions/projectActions";
import {ProjectHeader} from "./ProjectHeader";
import {TasksController} from "./TasksController";
import {ProjectDialog, ProjectForm} from "./ProjectDialog";
import AddIcon from "@material-ui/icons/Add";
import {mapAddProjectForm} from "../api/mappers";
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles({
    root: {
        marginTop: 5,
        backgroundColor: "green",
    },
});


const App: React.FC = () => {
    const [value, setValue] = React.useState(0);
    const [open, setOpen] = React.useState<boolean>(false);
    const dispatch = useDispatch();
    const classes = useStyles();

    const handleTabsChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleAddProject = (addProjectForm: ProjectForm): void => {
        dispatch(addProject(mapAddProjectForm(addProjectForm)))
    };

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        dispatch(loadProjects());
    }, [dispatch]);

    const {projects} = useSelector<AppStore, ProjectsState>(projectsSelector)

    return (
        <>
            <AppBar position="sticky" color="primary">
                <Tabs
                    value={value}
                    onChange={handleTabsChange}
                    indicatorColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                >
                    {projects.map((project: Project, index) => (
                        <ProjectHeader projectId={project.id} key={project.id} projectName={project.name}
                                       index={index}/>
                    ))}
                    <Fab size={"small"} className={classes.root} color="primary" onClick={handleClickOpen}>
                        <AddIcon/>
                    </Fab>
                    <ProjectDialog
                        open={open}
                        handleSubmit={handleAddProject}
                        handleClose={handleClose}
                        dialogTitle={"Add Project"}
                        submitButtonTitle={"Add"}/>
                </Tabs>
            </AppBar>
            {projects.map((project: Project, index) => (
                <TasksController key={project.id} projectId={project.id} tasks={project.tasks} index={index}
                                 value={value}/>
            ))}
        </>
    );
};

export default App;
