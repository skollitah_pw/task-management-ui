export interface AppStore {
    projectState: ProjectsState;
    apiState: ApiState;
}

export interface ProjectsState {
    projects: Project[];
}

export interface Project {
    id: string;
    name: string;
    created_at?: Date;
    last_updated_at?: Date;
    tasks: Task[];
}

export interface Task {
    id: string;
    name: string;
    details: string;
    status: TaskStatusEnum;
    created_at?: Date;
    last_updated_at?: Date;
}

export enum TaskStatusEnum {
    TO_DO = 'To Do',
    IN_PROGRESS = 'In Progress',
    DONE = 'Done',
    HOLD = 'Hold',
}

export interface ApiState {
    callsInProgress: number;
}