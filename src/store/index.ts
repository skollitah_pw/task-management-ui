import {applyMiddleware, createStore, Store} from 'redux'
import {rootReducer} from '../reducers';
import thunk from 'redux-thunk'
import {AppStore} from "./model";
import {composeWithDevTools} from 'redux-devtools-extension/logOnlyInProduction';

export const configureStore = (initState?: AppStore): Store<AppStore> => {
    return createStore(rootReducer, initState, composeWithDevTools(applyMiddleware(thunk)));
};
