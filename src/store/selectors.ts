import {createSelector} from 'reselect';
import {AppStore, ProjectsState} from "./model";

export const projectsSelector = createSelector<AppStore, any, ProjectsState>(
    (store: AppStore) => ({
        projects: store.projectState.projects
    }),
    ({projects}) => ({
        projects
    })
);