import {action, ActionType} from "typesafe-actions";

export enum ApiActions { // TODO api call finish
    API_CALL_START = 'API_CALL_START',
    API_CALL_FAILURE = 'API_CALL_FAILURE',
}

export const apiCallStart = () =>
    action(ApiActions.API_CALL_START);

export const apiCallFailure = (message: string) =>
    action(ApiActions.API_CALL_FAILURE, {message: message});

const apiActions = {
    apiCallStart: apiCallStart,
    apiCallFailure: apiCallFailure,
};

export type ApiActionTypes = ActionType<typeof apiActions>;

