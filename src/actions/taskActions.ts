import {action, ActionType} from "typesafe-actions";
import {StoreDispatch, StoreThunkAction} from "../reducers";
import {
    callAddTask,
    callCompleteTask,
    callDeleteTask,
    callEditTask,
    callInProgressTask,
    callOnHoldTask,
    callToDoTask
} from "../api/api";
import {ApiActionTypes, apiCallFailure, apiCallStart} from "./apiActions";
import {AddTaskDTO, TaskDTO} from "../api/model";

export enum TaskActions {
    ADD_TASK_SUCCESS = 'LOAD_PROJECTS_REQUEST',
    COMPLETE_TASK_SUCCESS = 'COMPLETE_TASK_SUCCESS',
    TO_DO_TASK_SUCCESS = 'TO_DO_TASK_SUCCESS',
    IN_PROGRESS_TASK_SUCCESS = 'IN_PROGRESS_TASK_SUCCESS',
    ON_HOLD_TASK_SUCCESS = 'ON_HOLD_TASK_SUCCESS',
    DELETE_TASK_SUCCESS = 'DELETE_TASK_SUCCESS',
    EDIT_TASK_SUCCESS = 'EDIT_TASK_SUCCESS',
}

const addTaskSuccess = (projectId: string, task: TaskDTO) =>
    action(TaskActions.ADD_TASK_SUCCESS, {projectId, task});

const completeTaskSuccess = (projectId: string, taskId: string) =>
    action(TaskActions.COMPLETE_TASK_SUCCESS, {projectId, taskId});

const toDoTaskSuccess = (projectId: string, taskId: string) =>
    action(TaskActions.TO_DO_TASK_SUCCESS, {projectId, taskId});

const inProgressTaskSuccess = (projectId: string, taskId: string) =>
    action(TaskActions.IN_PROGRESS_TASK_SUCCESS, {projectId, taskId});

const onHoldTaskSuccess = (projectId: string, taskId: string) =>
    action(TaskActions.ON_HOLD_TASK_SUCCESS, {projectId, taskId});

const deleteTaskSuccess = (projectId: string, taskId: string) =>
    action(TaskActions.DELETE_TASK_SUCCESS, {projectId, taskId});

const editTaskSuccess = (projectId: string, taskId: string, task: TaskDTO) =>
    action(TaskActions.EDIT_TASK_SUCCESS, {projectId, taskId, task});


export const addTask = (projectId: string, task: AddTaskDTO): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            const response = await callAddTask(projectId, task);
            return dispatch(addTaskSuccess(projectId, response));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding task'));
        }
    };
};

export const completeTask = (projectId: string, taskId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            await callCompleteTask(taskId);
            return dispatch(completeTaskSuccess(projectId, taskId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding task'));
        }
    };
};

export const toDoTask = (projectId: string, taskId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            await callToDoTask(taskId);
            return dispatch(toDoTaskSuccess(projectId, taskId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding task'));
        }
    };
};

export const inProgressTask = (projectId: string, taskId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            await callInProgressTask(taskId);
            return dispatch(inProgressTaskSuccess(projectId, taskId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding task'));
        }
    };
};

export const onHoldTask = (projectId: string, taskId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            await callOnHoldTask(taskId);
            return dispatch(onHoldTaskSuccess(projectId, taskId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding task'));
        }
    };
};

export const deleteTask = (projectId: string, taskId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        debugger;
        dispatch(apiCallStart());
        try {
            await callDeleteTask(taskId);
            return dispatch(deleteTaskSuccess(projectId, taskId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while deleting task'));
        }
    };
};

export const editTask = (projectId: string, taskId: string, task: AddTaskDTO): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<TaskActionTypes | ApiActionTypes> => {
        debugger;
        dispatch(apiCallStart());
        try {
            const response = await callEditTask(taskId, task);
            return dispatch(editTaskSuccess(projectId, taskId, response));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while deleting task'));
        }
    };
};

const taskActions = {
    addTaskSuccess: addTaskSuccess,
    completeTaskSuccess: completeTaskSuccess,
    toDoTaskSuccess: toDoTaskSuccess,
    inProgressTaskSuccess: inProgressTaskSuccess,
    onHoldTaskSuccess: onHoldTaskSuccess,
    deleteTaskSuccess: deleteTaskSuccess,
    editTaskSuccess: editTaskSuccess,
};

export type TaskActionTypes = ActionType<typeof taskActions>;

