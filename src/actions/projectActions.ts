import {AddProjectDTO, ProjectDTO} from "../api/model";
import {callAddProject, callDeleteProject, callEditProject, callGetAllProjects} from "../api/api";
import {action, ActionType} from 'typesafe-actions';
import {StoreDispatch, StoreThunkAction} from "../reducers";
import {ApiActionTypes, apiCallFailure, apiCallStart} from "./apiActions";

export enum ProjectActions {
    LOAD_PROJECTS_SUCCESS = 'LOAD_PROJECTS_SUCCESS',
    ADD_PROJECT_SUCCESS = 'ADD_PROJECT_SUCCESS',
    DELETE_PROJECT_SUCCESS = 'DELETE_PROJECT_SUCCESS',
    EDIT_PROJECT_SUCCESS = 'EDIT_PROJECT_SUCCESS',
}

const loadProjectsSuccess = (projects: ProjectDTO[]) =>
    action(ProjectActions.LOAD_PROJECTS_SUCCESS, {dto: projects});

const addProjectsSuccess = (project: ProjectDTO) =>
    action(ProjectActions.ADD_PROJECT_SUCCESS, {project});

const editProjectsSuccess = (projectId: string, projectDTO: ProjectDTO) =>
    action(ProjectActions.EDIT_PROJECT_SUCCESS, {projectId, projectDTO});

const deleteProjectsSuccess = (projectId: string) =>
    action(ProjectActions.DELETE_PROJECT_SUCCESS, {projectId});

export const loadProjects = (): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<ProjectActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            const response = await callGetAllProjects();
            return dispatch(loadProjectsSuccess(response));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while fetching projects'));
        }
    };
};

export const addProject = (project: AddProjectDTO): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<ProjectActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            const response = await callAddProject(project);
            return dispatch(addProjectsSuccess(response));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while adding project'));
        }
    };
};

export const deleteProject = (projectId: string): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<ProjectActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            await callDeleteProject(projectId);
            return dispatch(deleteProjectsSuccess(projectId));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while deleting project'));
        }
    };
};

export const editProject = (projectId: string, addProjectDTO: AddProjectDTO): StoreThunkAction => {
    return async (dispatch: StoreDispatch): Promise<ProjectActionTypes | ApiActionTypes> => {
        dispatch(apiCallStart());
        try {
            const response = await callEditProject(projectId, addProjectDTO);
            return dispatch(editProjectsSuccess(projectId, response));
        } catch (e) {
            return dispatch(apiCallFailure('Error occurred while deleting project'));
        }
    };
};

const projectActions = {
    loadProjectsSuccess: loadProjectsSuccess,
    addProjectsSuccess: addProjectsSuccess,
    deleteProjectsSuccess: deleteProjectsSuccess,
    editProjectsSuccess: editProjectsSuccess,
};

export type ProjectActionTypes = ActionType<typeof projectActions>;



